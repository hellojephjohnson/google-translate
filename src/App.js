import React, { useState, useEffect, Fragment } from "react";
import { googleTranslate } from "./utils/googleTranslate";
import "./styles.css";
import jsonData from './json/data.json';

const Items = ({ headline, body }) => {
  return (
    <div>
      <h3 className="catalog-card__heading">{headline}</h3>
      <p className="catalog-card__summary">{body}</p>
    </div>
  );
};

const Cards = ({ list }) => {
  return (
    list.map((item, index) => (
      <Fragment key={index}>
        <Items
          headline={item.headline}
          body={item.body}
        />
      </Fragment>
    ))
  )
};

const Options = ({ list }) => {
  return (
    list.map(lang => (
      <option 
        key={lang.language} 
        value={lang.language}
      >
        {lang.name}
      </option>
    ))
  )
};

export default function App() {
  const [languageCodes, setLanguageCodes] = useState([])
  const [language, setLanguage] = useState("en")
  const [question, setQuestion] = useState([...jsonData])
  const [welcome, setWelcome] = useState("Our Company")
  const [body, setBody] = useState("PepsiCo is one of the world’s leading food and beverage companies with over $63 billion in net revenue in 2015 and a global portfolio of diverse and beloved brands. Learn more about PepsiCo.")

  useEffect(() => {
    googleTranslate.getSupportedLanguages("en", (err, languageCodes) => {
      //console.log(languageCodes)
      const codesArray = [languageCodes[21], languageCodes[88], languageCodes[74], languageCodes[77], languageCodes[14]]
      getLanguageCodes(codesArray);
    });
    const getLanguageCodes = languageCodes => {
      setLanguageCodes(languageCodes)
    };
  },[])
  
  const changeHandler = (language) => {
    const translating = (transQuestion, originalText) => {
      if (question !== transQuestion) {
        setQuestion([{
            headline: originalText[0],
            body: originalText[1]
          },
          {
            headline: transQuestion[2],
            body: transQuestion[3]
          },
          {
            headline: jsonData[2].headline,
            body: jsonData[2].body
          },
          {
            headline: originalText[4],
            body: originalText[5]
          },
          {
            headline: transQuestion[4],
            body: transQuestion[5]
          },
          {
            headline: jsonData[5].headline,
            body: jsonData[5].body
          },
          {
            headline: originalText[8],
            body: originalText[9]
          },
          {
            headline: transQuestion[8],
            body: transQuestion[9]
          },
          {
            headline: jsonData[8].headline,
            body: jsonData[8].body
          },
          {
            headline: originalText[12],
            body: originalText[13]
          },
          {
            headline: transQuestion[12],
            body: transQuestion[13]
          },
          {
            headline: jsonData[11].headline,
            body: jsonData[11].body
          },
        ])
        setWelcome(transQuestion[16])
        setBody(transQuestion[17])
      }
    };

    if (language) {
      const questions = jsonData.map(text => [text.headline, text.body])
      googleTranslate.translate([...questions[0], ...questions[1], ...questions[3], ...questions[4], ...questions[7], ...questions[8], ...questions[10], ...questions[11], welcome, body], language, (err, translation) => {
        const questions = translation.map(text => text.translatedText)
        const originalText = translation.map(text => text.originalText)
        console.log("questions----", questions)
        console.log("originalText----", originalText)
        translating(questions, originalText)
      });
    }
      setLanguage(language)
  };
  
  return (
     <div className="App">
       <h1>Translate Text</h1>
       <select
        className="select-language"
        value={language}
        onChange={e => changeHandler(e.target.value)}
      >
        <Options list={languageCodes} />
      </select>
       {/* <h1 className="catalogs-page__heading">Our Company</h1>
       <p className="welcome-body">PepsiCo is one of the world’s leading food and beverage companies with over $63 billion in net revenue in 2015 and a global portfolio of diverse and beloved brands. Learn more about PepsiCo.</p> */}
       <h1 className="catalogs-page__heading">{welcome}</h1>
       <p className="welcome-body">{body}</p>
       {/* <h1 className="catalogs-page__heading">Nuestra empresa</h1>
       <p className="welcome-body">PepsiCo es una de las empresas líderes mundiales del sector de alimentos y bebidas con más de 63 mil millones de dólares de ingresos netos en 2015 y una cartera global de una variedad de marcas muy valoradas. Obtén más información sobre PepsiCo.</p> */}
       <div className="card-wrapper">
         <Cards list={question} />
      </div>
   </div>
  ); 
}

